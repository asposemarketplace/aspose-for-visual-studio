## Explore latest .NET API examples using Aspose Visual Studio Plugin 2.0

Aspose Visual Studio Plugin is a great tool to quickly download and explore Aspose .NET API examples. It saves lots of time and efforts by providing very simple option to seamlessly select, download and open the latest example projects without you ever having to leave your Visual Studio IDE. The new improved 2.0 version provides the following features

* Supports Visual Studio 2010 and Visual Studio 2012.
* Easy to launch from File or Tools menu in Visual Studio
* Saves time and reduces learning curve
* Enables you to select and open latest Aspose .NET API examples hosted on Github.com
* Selected example project is automatically opened in Visual Studio with all required references set to give you a ready to run and explore environment.
* Automatically downloads the latest version of API libraries and examples whenever there are new updates on the server.

![](http://www.aspose.com/blogs/wp-content/uploads/2014/03/Aspose-Visual-Studio-Plugin.png)

## How to Install the Aspose Visual Studio Plugin?

Installing Aspose Visual Studio Plugin templates is pretty easy. These instructions assumes that you have already installed Visual Studio 2010 or higher.

The plugin is provided in the form of a MSI installer. You can download the installer from one of the following locations

1. Download from [ Visual Studio Gallery](http://visualstudiogallery.msdn.microsoft.com/7a6463a4-f797-413b-bfb3-97f3154acae0)
2. Download from [CodePlex](https://asposevs.codeplex.com/)

Once downloaded double click the MSI to open the installer and then follow the simple instructions to complete the installation.

**Note:** Please make sure to restart Visual Studio for the changes to take effect.

## How to use Aspose Visual Studio Plugin?

Once installed you can easy launch and use the interactive plugin as mentioned below

1. Open the plugin using &lsquo;File&rsquo; -&gt; &lsquo;New Aspose Example Project&rsquo;  

![](http://www.aspose.com/blogs/wp-content/uploads/2014/03/Aspose-Visual-Studio-Plugin-Launch-from-File-Menu.png)


2. If you are opening the plugin for the first time (please proceed to step 3 otherwise) you will see the API selection dialog as shown below. Please select the APIs you want to explore and then click continue. It will take few minutes to download the selected APIs and their required libraries  
![](http://www.aspose.com/blogs/wp-content/uploads/2014/03/Aspose-Visual-Studio-Plugin-API-selection-dialog.png)

![](http://www.aspose.com/blogs/wp-content/uploads/2014/03/Aspose-Visual-Studio-Plugin-API-download-screen.png)


3. You will be presented with the following dialog, where you can select/change the location where the example projects will be created and also select/change the APIs you want to explore

![](http://www.aspose.com/blogs/wp-content/uploads/2014/03/Aspose-Visual-Studio-Plugin-API-Example-Selection.png)


4. Select the example you want to explore and either double click to open it or click &lsquo;Continue&rsquo; to open the example project in Visual Studio.

![](http://www.aspose.com/blogs/wp-content/uploads/2014/03/Aspose-Visual-Studio-Plugin-example-opened-in-visual-studio.png)


## Video

Please check the video below to see this in action.   
[https://www.youtube.com/watch?v=FN-bLZgq8_U](https://www.youtube.com/watch?v=FN-bLZgq8_U)



## What is the use of Aspose .NET Products?

[Aspose](http://www.aspose.com) are file format experts and provide APIs and components for various file formats including MS Office, OpenOffice, PDF and Image formats. These APIs are available on a number of development platforms including .NET
 frameworks &ndash; the .NET frameworks starting from version 2.0 are supported. If you are a .NET developer, you can use Aspose’s native .NET APIs in your .NET applications to process various file formats in just a few lines of codes. All the Aspose
 APIs don’t have any dependency over any other engine. For example, you don’t need to have MS Office installed on the server to process MS Office files. Below is a list of products we support for .NET developers:


## Aspose.Cells for .NET

[![Aspose.Cells for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_cells-for-net.jpg)](http://www.aspose.com/.net/excel-component.aspx)

Using these APIs, the .NET developers can perform simple and complex operations over various spreadsheet formats including MS Excel and OpenOffice spreadsheets. The APIs also provide conversion and rendering
 features for these file formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/excel-component.aspx)


## Aspose.Words for .NET

[![Aspose.Words for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_words-for-net.jpg)](http://www.aspose.com/.net/word-component.aspx)

Using these APIs, the .NET developers can perform simple and complex operations over various word processing formats including MS Word and OpenOffice documents. The APIs also provide conversion and rendering
 features for these file formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/word-component.aspx)


## Aspose.Pdf for .NET

[![Aspose.PDF for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_pdf-for-net.jpg)](http://www.aspose.com/.net/pdf-component.aspx)

Using these APIs, the .NET developers can perform simple and complex operations over PDF files. The APIs also provide conversion and rendering features for these file formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/pdf-component.aspx)


## Aspose.Slides for .NET

[![Aspose.Slides for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_slides-for-net.jpg)](http://www.aspose.com/.net/powerpoint-component.aspx)

Using these APIs, the .NET developers can perform simple and complex operations over various presentation formats including MS PowerPoint and OpenOffice presentations. The APIs also provide conversion and rendering features for these file formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/powerpoint-component.aspx)


## Aspose.BarCode for .NET

[![Aspose.BarCode for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_barcode-for-net.jpg)](http://www.aspose.com/.net/barcode-component.aspx)

Using these APIs, the .NET developers can generate and recognize a variety of barcode symbologies. Create barcode applications, or add barcodes to documents using these APIs.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/barcode-component.aspx)


## Aspose.Tasks for .NET

[![Aspose.Tasks for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_tasks-for-net.jpg)](http://www.aspose.com/.net/project-management-component.aspx)

Using these APIs, the .NET developers can create, read, manipulate, convert and save Microsoft Project files. The APIs also provide conversion and rendering features for MS Project file formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/project-management-component.aspx)


## Aspose.Email for .NET

[![Aspose.Email for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_email-for-net.jpg)](http://www.aspose.com/.net/email-component.aspx)

Using these APIs, the .NET developers can perform simple and complex operations over various email formats including MS Outlook email formats. The APIs also provide conversion and rendering features for these file formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/email-component.aspx)


## Aspose.Diagram for .NET

[![Aspose.Diagram for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_diagram-for-net.jpg)](http://www.aspose.com/.net/diagram-component.aspx)

Using these APIs, the .NET developers can work with Microsoft Visio drawing files. The APIs enable developers to quickly create .NET applications for manipulating and converting Microsoft Visio drawing files.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/diagram-component.aspx)[](http://www.aspose.com/demos/.net-components/aspose.diagram/default.aspx)



## Aspose.OCR for .NET

[![Aspose.OCR for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_ocr-for-net.jpg)](http://www.aspose.com/.net/ocr-component.aspx)

Using these APIs, the .NET developers can perform OCR operations over images. The APIs hide all the complexities involved with OCR and developers only need to write few lines of codes to call public interfaces exposed by the API.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/ocr-component.aspx)[](http://www.aspose.com/demos/.net-components/aspose.ocr/default.aspx)


## Aspose.Imaging for .NET


[![Aspose.Imaging for .NET](http://www.aspose.com/App_Themes/V2/images/productLogos/NET/aspose_imaging-for-net.jpg)
](http://www.aspose.com/.net/imaging-component.aspx)

Using these APIs, the .NET developers can create, open, manipulate and save images of various formats.

[![Learn More](http://www.aspose.com/Images/Learn-More.gif)](http://www.aspose.com/.net/imaging-component.aspx)[](http://www.aspose.com/demos/.net-components/aspose.imaging/default.aspx)